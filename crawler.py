from datetime import datetime, timedelta
from mattermostdriver import Driver
import pathlib
import json
import os, re
import requests

token = os.environ.get('MMAUTHTOKEN')
user = os.environ.get('USER')
target_team = os.environ.get('TEAM')
target_channel = os.environ.get('CHANNEL')

def connect(host, username, login_token):
    # Connect to server
    d = Driver({
        "url": host,
        "login_id": username,
        "password": "blablabla",
        "token": login_token,
        "port": 443,
        "scheme": 'https',
        "timeout": 30
    })
    d.login()
    # Get all usernames as we want to use those instead of the user ids
    user_id_to_name = {}
    page = 0
    print("Downloading all user data")
    while True:
        users_resp = d.users.get_users(params={"per_page": 200, "page": page})
        if len(users_resp) == 0:
            break
        for user in users_resp:
            user_id_to_name[user["id"]] = user["username"]
        page += 1
    my_user_id = d.users.get_user_by_username(username)["id"]
    # print("Id of logged in user:", my_user_id)

    return d, user_id_to_name, my_user_id


def select_team(d, my_user_id, desired_team_id):
    teams = d.teams.get_user_teams(my_user_id)
    selected_team = None
    for team in teams:
        if team["id"] == desired_team_id:
            selected_team = team
            break
    if selected_team is None:
        raise ValueError("Desired team ID {} not found.".format(desired_team_id))
    print("Selected team:", selected_team["name"])
    return selected_team


def select_channel(d, team, my_user_id, user_id_to_name):
    channels = d.channels.get_channels_for_user(my_user_id, team["id"])
    # Add display name to direct messages
    for channel in channels:
        if channel["type"] != "D":
            continue

        # The channel name consists of two user ids connected by a double underscore
        user_ids = channel["name"].split("__")
        other_user_id = user_ids[1] if user_ids[0] == my_user_id else user_ids[0]
        channel["display_name"] = user_id_to_name[other_user_id]
    # Sort channels by name for easier search
    channels = sorted(channels, key=lambda x: x["display_name"].lower())
    print("Found Channels:")
    for i_channel, channel in enumerate(channels):
        if channel["id"] == target_channel:
            selected_channels = channel
            print("{}\t{}".format(i_channel, channel["display_name"]))
            break
    return selected_channels


def export_channel(d, channel, user_id_to_name, output_base, before=None, after=None):
    channel_name = str(channel["id"])

    print("Exporting channel", channel_name)
    if after:
        after = datetime.strptime(after, '%Y-%m-%d').timestamp()
    if before:
        before = datetime.strptime(before, '%Y-%m-%d').timestamp()

    # Get all posts for selected channel
    page = 0
    all_posts = []
    while True:
        print("Requesting channel page {}".format(page))
        posts = d.posts.get_posts_for_channel(channel["id"], params={"per_page": 200, "page": page})

        if len(posts["posts"]) == 0:
            # If no posts are returned, we have reached the end
            break

        all_posts.extend([posts["posts"][post] for post in posts["order"]])
        page += 1
    print("Found {} posts".format(len(all_posts)))
    # Create output directory
    output_base = pathlib.Path(output_base) / channel_name
    if not output_base.exists():
        output_base.mkdir()
    # Simplify all posts to contain only username, date, message and files in chronological order
    simple_posts = []
    for i_post, post in enumerate(reversed(all_posts)):

        # Filter posts by date range
        created = post["create_at"] / 1000
        if (before and created > before) or (after and created < after):
            continue

        user_id = post["user_id"]
        if user_id not in user_id_to_name:
            user_id_to_name[user_id] = d.users.get_user(user_id)["username"]
        username = user_id_to_name[user_id]
        created = datetime.utcfromtimestamp(post["create_at"] / 1000).strftime('%Y-%m-%dT%H:%M:%SZ')
        message = post["message"]
        simple_post = dict(id=i_post, created=created, username=username, message=message)

        # If a code block is given in the message, dump it to file
        if message.count("```") > 1:
            start_pos = message.find("```") + 3
            end_pos = message.rfind("```")

            cut = message[start_pos:end_pos]
            if not len(cut):
                print("Code cut has no length")
            else:
                filename = "%03d" % i_post + "_code.txt"
                with open(output_base / filename, "w") as f:
                    f.write(cut)

        # If any files are attached to the message, download each
        if "files" in post["metadata"]:
            filenames = []
            for file in post["metadata"]["files"]:
                if download_files:
                    filename = "%03d" % i_post + "_" + file["name"]
                    print("Downloading", file["name"])
                    resp = d.files.get_file(file["id"])
                    # Mattermost Driver unfortunately parses json files to dicts
                    if isinstance(resp, dict):
                        with open(output_base / filename, "w") as f:
                            json.dump(resp, f)
                    else:
                        with open(output_base / filename, "wb") as f:
                            f.write(resp.content)

                filenames.append(file["name"])
            simple_post["files"] = filenames
        simple_posts.append(simple_post)

    # Export posts to json file
    output_filename = channel_name + ".json"
    os.environ["JSON_PATH"] = str(output_base) + '/' + str(output_filename)
    with open(output_base / output_filename, "w", encoding='utf8') as f:
        json.dump(simple_posts, f, indent=2, ensure_ascii=False)
    print("Dumped channel texts to", output_filename)


def find_questions(json_file, exclude_users=[]):
    with open(json_file, 'r') as file:
        data = json.load(file)

    questions = []
    for item in data:
        username = item.get('username', '')
        if username in exclude_users:
            # print("Skipping user: ",username)
            continue
        message = item.get('message', '')
        # Using regular expression to find sentences ending with a question mark
        if re.search(r'[?]|(what|how|why|should|could|improve)', message, re.IGNORECASE):
            # print(question_matches)
            questions.append(message)

    return questions


if __name__ == '__main__':
    host = "mattermost.web.cern.ch"
    username = user  # Your gitlab username
    login_token = token  # Access Token. Can be extracted from Browser Inspector (MMAUTHTOKEN)
    output_base = "results/"
    if not os.path.exists(output_base):
        os.makedirs(output_base)
    download_files = True

    # Range of posts to be exported as string in format "YYYY-MM-DD". Use None if no filter should be applied
    today = datetime.today().date()

    after = (today - timedelta(days=15)).strftime("%Y-%m-%d")
    before = today.strftime("%Y-%m-%d")

    d, user_id_to_name, my_user_id = connect(host, username, login_token)
    team = select_team(d, my_user_id, desired_team_id=target_team)
    channels = select_channel(d, team, my_user_id, user_id_to_name)
    export_channel(d, channels, user_id_to_name, output_base, before, after)
    print("Finished export")


###################################

json_file = os.environ["JSON_PATH"]
exclude_users = ['gguerrie', 'bejones', 'etejedor', 'sciaba']
questions = "You are creating a FAQ section. \
    Summarise from the list of sentences below the most relevant questsions or suggestions for improvement \
    If you don't know what to write, do not create content, just don't write anything. \
    Report at maximum 10 items, and only if needed. Try to condense the content: the less bullets, the better. Here's the list: " \
        + str(find_questions(json_file,exclude_users))

# print(str(questions))

url = "http://itgptllm.cern.ch:8080"
headers = {"Content-Type": "application/json"}
data = {"question": questions}

response = requests.post(url, headers=headers, data=json.dumps(data))
output = response.json()['output'].replace('\n', '\n\n')  # Convert newline to double newline for better formatting

# Write the output to a text file
with open('output.txt', 'w') as file:
    file.write(output)

print("Status Code:", response.status_code)
print("Response Body:", response.json())